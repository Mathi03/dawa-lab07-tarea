const express = require('express');
const Publication = require('../models/publication');
const {verificaToken} = require('../middlewares/authentication');
const {v4: uuidv4} = require('uuid');
const path = require('path');
const {nextTick} = require('process');

const app = express();

app.post('/publicacion', verificaToken, function (req, res) {
  let body = req.body;

  let archivo = req.files.image;
  //console.log(archivo);
  let nombreArchivo = uuidv4() + path.extname(archivo.name).toLocaleLowerCase();

  archivo.mv(`public/images/${nombreArchivo}`, (err) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err,
      });
    }
  });

  let publication = new Publication({
    title: body.title,
    subtitle: body.subtitle,
    imagePath: 'public/images/' + nombreArchivo,
    //user_id: body.user_id
  });

  publication.save((err, usuarioBD) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }

    usuarioBD.password = null;

    res.json({
      ok: true,
      usuario: usuarioBD,
    });
  });
});

app.get('/publicacion', async function (req, res) {
  const Publications = await Publication.find();
  res.json(Publications);
});
const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
app.put('/publicacion/:id', verificaToken, async (req, res) => {
  let id = req.params.id;
  let fields = {
    title: req.body.title,
    subtitle: req.body.subtitle,
  };
  if (req.files) {
    let archivo = req.files.image;
    //console.log(archivo);
    let nombreArchivo =
      uuidv4() + path.extname(archivo.name).toLocaleLowerCase();

    archivo.mv(`public/images/${nombreArchivo}`, (err) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          err,
        });
      }
    });

    fields.imagePath = 'public/assets/images/' + nombreArchivo;
  }
  await Publication.findByIdAndUpdate(id, {$set: fields});
  res.json({status: 'Task Updated', fields});
});

app.get('/publicacion_id/:id', verificaToken, async (req, res) => {
  const publication = await Publication.findById(req.params.id);
  res.json(publication);
});
module.exports = app;

/* app.delete('/publicacion/', async (req, res) => {
  await Publication.findByIdAndRemove(req.body.id);
  res.json({status: 'Task Deleted'});
}); */

app.delete('/publicacion/:id', verificaToken, function (req, res) {
  let id = req.params.id;

  Publication.findByIdAndRemove(id, (err, usuarioBorrado) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err,
      });
    }
    res.json({
      ok: true,
      usuario: usuarioBorrado,
    });
  });
});
