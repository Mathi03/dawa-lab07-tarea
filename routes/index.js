const express = require('express');

const app = express();

app.use('/api', require('./usuario'));
app.use('/api', require('./publication'));
app.use('/api', require('./restaurante.routes'));
app.use(require('./login'));

app.use(require('./upload'));
app.use(require('./images'));

module.exports = app;
